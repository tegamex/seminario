import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.websocket
import base64
import websocket
import threading
import cStringIO
import base64
import cv2
import ast
import numpy
from time import sleep
from PIL import Image
from base64 import decodestring
from db import db

class WebSocketHandler( tornado.websocket.WebSocketHandler ):

    def initialize( self ):
        pass
      
    def open( self ):
        pass
      
    def on_message( self , message ):
        from server import detect
        img_data = Image.frombytes( 'RGB' , ( 416 , 416 ) , decodestring( message ) )
        img_data = numpy.array( img_data )
        result = detect( img_data )
        self.write_message( repr( result ) )

    def on_close( self ):
        pass

class WebSocketHandlerClient( websocket.WebSocketApp ):
    def __init__( self , ip ):
        websocket.WebSocketApp.__init__( self , ip , on_message = self.on_message , on_error = self.on_error , on_close = self.on_close )
        self.result = None

    def on_open( self , ws ):
        pass

    def on_message( self , ws , message ):
        self.result = message

    def on_error( self , ws , error ):
        pass

    def on_close( self , ws ):
        self.result = "[]"
        pass

    def recv( self ):
        while self.result is None:
            sleep( 0.001 )
        result = self.result
        self.result = None
        return result 

class ConnectionServer( tornado.websocket.WebSocketHandler ):
    def __init__( self ):
        pass

    def start_server( self , port = 8888 ):
        application = tornado.web.Application( [ ( r'/ws' , WebSocketHandler ) ] )
        http_server = tornado.httpserver.HTTPServer( application )
        http_server.listen( port )
        tornado.ioloop.IOLoop.instance().start()

    def connect_server( self , ip , port = 8888 ):
        websocket.enableTrace( False )
        self.ws = WebSocketHandlerClient( "ws://%s:%s/ws" % ( ip , port ) )
        wst = threading.Thread( target = self.ws.run_forever )
        wst.daemon = True
        wst.start()
        conn_timeout = 5
        while not self.ws.sock.connected and conn_timeout:
            sleep( 0.5 )
            conn_timeout -= 1

    def send_image( self , img ):
        if not self.ws.sock or not self.ws.sock.connected:
            print "no connected it seems"
            return None
        if img.shape[ 0 ] != 416 and img.shape[ 1 ] != 416:
            img = cv2.resize( img , ( 416 , 416 ) )
            return self.send_image( img )
        self.ws.send( base64.b64encode( img ) )
        result =  self.ws.recv()
        result = ast.literal_eval( result )
        return result

    def close_connection( self ):
        if self.ws:
            self.ws.close()
        else:
            print "I'm server it seems"


