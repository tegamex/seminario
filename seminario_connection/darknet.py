from ctypes import *
from db import db
import math
import random
import cv2
import numpy

def sample(probs):
    s = sum(probs)
    probs = [a/s for a in probs]
    r = random.uniform(0, 1)
    for i in range(len(probs)):
        r = r - probs[i]
        if r <= 0:
            return i
    return len(probs)-1

def c_array(ctype, values):
    return (ctype * len(values))(*values)

class BOX(Structure):
    _fields_ = [("x", c_float),
                ("y", c_float),
                ("w", c_float),
                ("h", c_float)]

class IMAGE(Structure):
    _fields_ = [("w", c_int),
                ("h", c_int),
                ("c", c_int),
                ("data", POINTER(c_float))]

class METADATA(Structure):
    _fields_ = [("classes", c_int),
                ("names", POINTER(c_char_p))]

try:
    lib = CDLL("lib/libdarknet.so", RTLD_GLOBAL)
except OSError:
    print( "No lib file" )
    assert( 0 )
lib.network_width.argtypes = [c_void_p]
lib.network_width.restype = c_int
lib.network_height.argtypes = [c_void_p]
lib.network_height.restype = c_int

predict = lib.network_predict
predict.argtypes = [c_void_p, POINTER(c_float)]
predict.restype = POINTER(c_float)

set_gpu = lib.cuda_set_device
set_gpu.argtypes = [c_int]

make_image = lib.make_image
make_image.argtypes = [c_int, c_int, c_int]
make_image.restype = IMAGE

make_boxes = lib.make_boxes
make_boxes.argtypes = [c_void_p]
make_boxes.restype = POINTER(BOX)

free_ptrs = lib.free_ptrs
free_ptrs.argtypes = [POINTER(c_void_p), c_int]

num_boxes = lib.num_boxes
num_boxes.argtypes = [c_void_p]
num_boxes.restype = c_int

make_probs = lib.make_probs
make_probs.argtypes = [c_void_p]
make_probs.restype = POINTER(POINTER(c_float))

detect = lib.network_predict
detect.argtypes = [c_void_p, IMAGE, c_float, c_float, c_float, POINTER(BOX), POINTER(POINTER(c_float))]

reset_rnn = lib.reset_rnn
reset_rnn.argtypes = [c_void_p]

load_net = lib.load_network
load_net.argtypes = [c_char_p, c_char_p, c_int]
load_net.restype = c_void_p

free_image = lib.free_image
free_image.argtypes = [IMAGE]

letterbox_image = lib.letterbox_image
letterbox_image.argtypes = [IMAGE, c_int, c_int]
letterbox_image.restype = IMAGE

load_meta = lib.get_metadata
lib.get_metadata.argtypes = [c_char_p]
lib.get_metadata.restype = METADATA

load_image = lib.load_image_color
load_image.argtypes = [c_char_p, c_int, c_int]
load_image.restype = IMAGE

rgbgr_image = lib.rgbgr_image
rgbgr_image.argtypes = [IMAGE]

predict_image = lib.network_predict_image
predict_image.argtypes = [c_void_p, IMAGE]
predict_image.restype = POINTER(c_float)

network_detect = lib.network_detect
network_detect.argtypes = [c_void_p, IMAGE, c_float, c_float, c_float, POINTER(BOX), POINTER(POINTER(c_float))]

def predict( net , im ):
    out = predict_image(net, im)
    res = []
    for i in range( 2 ):
        res.append( ( i , out[ i ] ) )
    res = sorted( res, key = lambda x : -x[ 1 ] )
    return res

def numpy_to_array( src , out ):
    ( w , h , c ) = ( src.shape[ 1 ] , src.shape[ 0 ] , src.shape[ 2 ] )
    for i in range( h ):
        for j in range( w ):
            for k in range( c ):
                out[ k * w * h + i * w + j ] = src[ i ][ j ][ k ]

def swap_channels( src , channel_size ):
    for i in range( channel_size ):
        swap = src[ i ]
        src[ i ] = src[ i + channel_size * 2 ]
        src[ i + channel_size * 2 ] = swap

class Darknet:
    def __init__( self ):
        self.net = None
        self.meta_data = None
        self.boxes = None
        self.probs = None
        self.max_boxes = None

    def load_net( self , net_file , weight_file ):
        self.net = load_net( net_file , weight_file , 0 )
        self.boxes = make_boxes( self.net )
        self.probs = make_probs( self.net)
        self.max_boxes = num_boxes( self.net )

    def load_meta( self , meta_file ):
        self.meta_data = load_meta( meta_file )

    def detect( self , img_data , thresh = .24 , hier_thresh = .5 , nms = .45 ):
        assert self.net , "Net no loaded"
        assert type( img_data ) is numpy.ndarray
        img_data = img_data / 255.
        ( w , h , c ) = ( img_data.shape[ 1 ] , img_data.shape[ 0 ] , img_data.shape[ 2 ] )
        im = make_image( w , h , c )
        numpy_to_array( img_data , im.data )
        swap_channels( im.data , w * h )
        result = predict_image( self.net , im )
        network_detect( self.net , im , thresh , hier_thresh , nms , self.boxes , self.probs )
        res = []
        for j in range( self.max_boxes ):
            for i in range( 2 ): #meta.classes ):
                if self.probs[ j ][ i ] > 0:
                    if self.meta_data:
                        id_name = self.meta_data.names[ i ]
                    else:
                        id_name = str( i )
                    res.append( ( id_name , self.probs[ j ][ i ], self.boxes[ j ].x / w , self.boxes[ j ].y / h , self.boxes[ j ].w / w , self.boxes[ j ].h / h ) )
        res = sorted( res , key = lambda x: -x[ 1 ] )
        free_image( im )
        #free_ptrs( cast( probs , POINTER( c_void_p ) ) , num )
        return res
