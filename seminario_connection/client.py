import cv2
import threading
import ast
from time import sleep , time
from db import db
def run( args ):
    if args.ip:
        run_from_server( args )
    else:
        run_from_local( args )

def run_from_local( args ):
    if not args.net or not args.weights:
        args.parser.print_help()
        args.parser.error( "Insert path of net and weights" )
    try:
        from darknet import Darknet
    except ImportError:
        print "No found library"
        assert( False )

    
    model = Darknet()
    model.load_net( args.net , args.weights )
    
    if args.metadata:
        model.load_meta( args.metadata )

    setattr( args , "detector" , model.detect )
    if args.file:
        run_from_file( args )
    else:
        run_from_camera( args )

def run_from_server( args ):
    from connection_server import ConnectionServer
    connection = ConnectionServer()
    connection.connect_server( args.ip )
    setattr( args , "detector" , connection.send_image )
    if args.file:
        run_from_file( args )
    else:
        run_from_camera( args )
    connection.close_connection()
    
def draw_results( img_data , boxes , name_classes = None ):

    width = img_data.shape[ 1 ]
    height = img_data.shape[ 0 ]

    for i in range( len( boxes ) ):
        box = boxes[ i ]

        x1 = int( ( box[ 2 ] - box[ 4 ] / 2.0 ) * width )
        y1 = int( ( box[ 3 ] - box[ 5 ] / 2.0 ) * height )
        x2 = int( ( box[ 2 ] + box[ 4 ] / 2.0 ) * width )
        y2 = int( ( box[ 3 ] + box[ 5 ] / 2.0 ) * height )

        rgb = ( 0 , 0 , 0 )
        cls_conf = box[ 1 ]
        cls_id   = box[ 0 ]

        cv2.putText( img_data , str( cls_id ) , ( x1 , y1 ) , cv2.FONT_HERSHEY_SIMPLEX , 1.2 , rgb , 1 )
        cv2.rectangle( img_data , ( x1 , y1 ) , ( x2 , y2 ) , rgb , 2 )
    return img_data

def run_from_camera( args ):
    cap = cv2.VideoCapture( int( args.cam ) )
    if not cap.isOpened():
        if not run_from_picamera( args ):
            args.parser.print_help()
            args.parser.error( "Couldn't connect with camera" )
        return
    start = time()
    while( cap.isOpened() ):
        ret, img_bytes = cap.read()
        if ret == False:
            print "Error reading from camera"
            break
        result = args.detector( img_bytes )
        if result is None:
            return
        img_bytes = draw_results( img_bytes , result )
        print "%s FPS" % ( 1./ ( time() - start ) )
        start = time()
        cv2.imshow( "image" , img_bytes )
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()

def run_from_picamera( args ):
    try:
        from picamera.array import PiRGBArray
        from picamera import PiCamera
    except ImportError:
        return False
     
    camera = PiCamera()
    camera.resolution = ( 416 , 416 )
    camera.framerate = 32
    rawCapture = PiRGBArray( camera , size=( 416 , 416 ) )
     
    sleep( 0.1 )

    start = time()
    for frame in camera.capture_continuous( rawCapture , format="bgr" , use_video_port = True ):
        img_bytes = frame.array
        result = args.detector( img_bytes )
        if result is None:
            return
        img_bytes = draw_results( img_bytes , result )
        print "%s FPS" % ( 1./ ( time() - start ) )
        start = time()
        #cv2.imshow( "image" , img_bytes )
        #cv2.imwrite( "%s.jpg" % ( time() ) , img_bytes )
        if cv2.waitKey( 1 ) & 0xFF == ord( 'q' ):
            break
        rawCapture.truncate( 0 )

    cv2.destroyAllWindows()
    return True

def run_from_file( args ):
    img_bytes = cv2.imread( args.file )
    if img_bytes is None:
        return
    result = args.detector( img_bytes )
    if result is None:
        return
    img_bytes = draw_results( img_bytes , result )
    cv2.imshow( "image" , img_bytes )
    #cv2.imwrite( "%s.jpg" % ( time() ) , img_bytes )
    cv2.waitKey( 0 )
    cv2.destroyAllWindows()
