from darknet import Darknet
from db import db
from connection_server import ConnectionServer

detect = None

def run( args ):
    global detect
    assert args.net and args.weights 
    model = Darknet()
    model.load_net( args.net , args.weights )
    if args.metadata:
        model.load_meta( args.metadata )
    detect = model.detect
    connection = ConnectionServer()
    connection.start_server()
