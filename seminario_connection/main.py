import argparse
def main():
    parser = argparse.ArgumentParser( formatter_class = argparse.RawTextHelpFormatter )
    parser.add_argument( "-s" , "--side" , dest = "side" , help = "Is client or server?" , default = "server" )
    #parser.add_argument( "-l" , "--lib" , dest = "lib" , help =
    #                    "Path of libdarknet.so",
    #                    default = "libdarknet.so" )
    parser.add_argument( "-c" , "--cam" , dest = "cam" , help = "Index of camera" , default = "0" )
    parser.add_argument( "-n" , "--net" , dest = "net" , help = "path of net config" )
    parser.add_argument( "-w" , "--weights" , dest = "weights" , help = "path of weights file" )
    parser.add_argument( "-d" , "--metadata" , dest = "metadata" , help = "config of metadata" )
    parser.add_argument( "-i" , "--ip" , dest = "ip" , help = "Ip of server" )
    parser.add_argument( "-f" , "--file" , dest = "file" , help = "Test File" )
    args = parser.parse_args()
    setattr( args , "parser" , parser )
    if args.side == "client":
        from client import run
    elif args.side == "server":
        from server import run
    else:
        parser.print_help()
        parser.error( "Side not valid" )
    run( args )

if __name__ == "__main__":
    main()
