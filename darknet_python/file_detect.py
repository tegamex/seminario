import cv2
import os
from bk import db
from PIL import Image
import urllib , cStringIO 
from convolutional_network import ConvNet
from utils import *

def detect( args ):
    if args.path.startswith( "http" ):
        detect_function = detect_from_url
    elif os.path.isdir( args.path ):
        detect_function = detect_from_directory
    elif os.path.isfile( args.path ):
        detect_function = detect_from_file
    else:
        args.parser.print_help()
        args.parser.error( "Error in path" )

    model_file     = parse_model_file( args.model )

    network_option = model_file[ 0 ]
    loss_option    = model_file[ -1 ]
    assert network_option[ "type" ] == "net" , "Error in model file"
    assert loss_option[ "type" ] in { "cost" , "region" } , "Error in model_file"

    data_conf                  = dict()
    data_conf[ "size" ]        = ( int( network_option[ 'width' ] ) , int( network_option[ 'height' ] ) )
    data_conf[ "anchors" ]     = [ float( i ) for i in loss_option[ 'anchors' ].split( ',' ) ]
    data_conf[ "num_anchors" ] = int( loss_option[ 'num' ] )
    data_conf[ "num_classes" ] = int( loss_option[ 'classes' ] )
    data_conf[ "nms_thresh" ]  = 0.5
    data_conf[ "conf_thresh" ] = 0.4

    detect_function( args , data_conf )

def detect_from_url( args ):
    file_url = cStringIO.StringIO( urllib.urlopen( path ).read() )
    img_bytes = Image.open( file_url )
    if img_bytes is None:
        args.parser.print_help()
        args.parser.error( "Error in path, bad connection or file is not image" )
        return
    cv2.imshow( "image" , img_bytes )
    cv2.waitKey( 0 )
    cv2.destroyAllWindows()

def detect_from_file( args , data_conf ):

    model = ConvNet( args )
    if args.cuda:
        model.cuda()

    img_bytes = cv2.imread( args.path )
    if img_bytes is None:
        return
    img_bytes = cv2.resize( img_bytes , data_conf[ "size" ] )
    img_bytes = detect_from_image( model , img_bytes , data_conf )
    cv2.imshow( "image" , img_bytes )
    cv2.waitKey( 0 )
    cv2.destroyAllWindows()

def detect_from_directory( args ):
    list_files = os.listdir( args.path )
    if not os.path.exists( '%s/output'%( args.path ) ):
        os.makedirs( '%s/output'%( args.path ) )
        
    if not os.path.isdir( '%s/output'%( args.path ) ):
        args.parser.error( "Delete directory 'output' in test directory" )
    
    for item_file in list_files:
        path = os.path.join( args.path , item_file )
        if not os.path.isfile( path ):
            continue
        img_bytes = cv2.imread( path )
        if img_bytes is None:
            continue
        path = os.path.join( args.path , 'output' , item_file )
        Image.fromarray( img_bytes ).save( path )
    
