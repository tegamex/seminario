from bk import db
def run( args ):
    if args.model is None:
        args.parser.print_help()
        args.parser.error( "Insert path of model" )
    if args.weights is None:
        args.parser.print_help()
        args.parser.error( "Insert path of weights" )
    if args.config is None:
        args.parser.print_help()
        args.parser.error( "Insert path of config" )
    try:
        num = int( args.path )
        from camera_detect import detect
    except ValueError:
        from file_detect import detect
    detect( args )
