import time
import torch
import math
import torch.nn as nn
import torch.nn.functional as F
from utils import *
from bk import db
has_cuda = torch.cuda.is_available()

def build_targets( pred_boxes , target , anchors , num_anchors , num_classes , output_height , output_width , noobject_scale , object_scale , sil_thresh ):
    target_count = target.size(0)
    anchor_step = len( anchors ) / num_anchors
    conf_no_object_mask = torch.ones( target_count , num_anchors , output_height , output_width ) * noobject_scale
    coord_mask          = torch.zeros( target_count , num_anchors , output_height , output_width )
    cls_mask            = torch.zeros( target_count , num_anchors , output_height , output_width )
    tx                  = torch.zeros( target_count , num_anchors , output_height , output_width )
    ty                  = torch.zeros( target_count , num_anchors , output_height , output_width )
    tw                  = torch.zeros( target_count , num_anchors , output_height , output_width )
    th                  = torch.zeros( target_count , num_anchors , output_height , output_width )
    tconf               = torch.zeros( target_count , num_anchors , output_height , output_width )
    tcls                = torch.zeros( target_count , num_anchors , output_height , output_width )

    count_pixels  = output_height * output_width 
    count_anchors = num_anchors * count_pixels
    for b in xrange( target_count ):
        current_pred_boxes = pred_boxes[ b * count_anchors : ( b + 1 ) * count_anchors ].t()
        current_cum_ious   = torch.zeros( count_anchors )
        for t in xrange( 50 ):
            if target[ b ][ t * 5 + 3 ] == 0 and target[ b ][ t * 5 + 4 ] == 0:
                break
            true_x           = target[ b ][ t * 5 + 1 ] * output_width
            true_y           = target[ b ][ t * 5 + 2 ] * output_height
            true_width       = target[ b ][ t * 5 + 3 ] * output_width
            true_height      = target[ b ][ t * 5 + 4 ] * output_height
            true_box         = torch.FloatTensor( [ true_x , true_y , true_width , true_height ] ).repeat( count_anchors , 1 ).t()
            current_ious     = iou_multiple_boxes( current_pred_boxes , true_box , is_centroid = True )
            current_cum_ious = torch.max( current_cum_ious , current_ious )
        conf_no_object_mask[ b ][ current_cum_ious > sil_thresh ] = 0
    if anchor_step == 4:
        tx = torch.FloatTensor( anchors ).view( num_anchors , anchor_step ).index_select( 1 , torch.LongTensor( [ 2 ] ) ).view( 1 , num_anchors , 1 , 1 ).repeat( target_count , 1 , output_height , output_width )
        ty = torch.FloatTensor( anchors ).view( num_anchors , anchor_step ).index_select( 1 , torch.LongTensor( [ 2 ] ) ).view( 1 , num_anchors , 1 , 1 ).repeat( target_count , 1 , output_height , output_width )
    else:
        tx.fill_( 0.5 )
        ty.fill_( 0.5 )
    if True :
        tw.zero_()
        th.zero_()
        coord_mask.fill_( 1 )  

    count_true_boxes = 0
    count_correct    = 0
    for target_index in xrange( target_count ):
        for t in xrange( 50 ):
            if target[ target_index ][ t * 5 + 3 ] == 0 and target[ b ][ t * 5 + 4 ] == 0:
                break

            count_true_boxes  = count_true_boxes + 1
            best_iou          = 0.0
            best_index_anchor = -1
            best_dist_anchor  = 10000
            true_x            = target[ target_index ][ t * 5 + 1 ] * output_width
            true_y            = target[ target_index ][ t * 5 + 2 ] * output_height 
            true_width        = target[ target_index ][ t * 5 + 3 ] * output_width
            true_height       = target[ target_index ][ t * 5 + 4 ] * output_height 
            true_grid_x       = int( true_y )
            true_grid_y       = int( true_x )
            true_box_size     = [ 0 , 0 , true_width , true_height ]

            for index_anchor in xrange( num_anchors ):
                anchor_width  = anchors[ anchor_step * index_anchor ]
                anchor_height = anchors[ anchor_step * index_anchor + 1 ]
                anchor_box    = [ 0 , 0 , anchor_width , anchor_height ]
                iou           = iou_single_boxes( anchor_box , true_box_size , is_centroid = True )

                if anchor_step == 4:
                    anchor_x = anchors[ anchor_step * index_anchor + 2 ]
                    anchor_y = anchors[ anchor_step * index_anchor + 3 ]
                    dist = pow( ( ( true_grid_x + anchor_x ) - true_x ) , 2 ) + pow( ( ( true_grid_y + anchor_y ) - true_y ) , 2 )

                if iou > best_iou:
                    best_iou          = iou
                    best_index_anchor = index_anchor
                elif anchor_step == 4 and iou == best_iou and dist < min_dist:
                    best_iou          = iou
                    best_index_anchor = index_anchor
                    min_dist_anchor   = dist

            true_box = [ true_x , true_y , true_width , true_height ]
            pred_box = pred_boxes[ target_index * count_anchors + best_index_anchor * count_pixels + true_grid_y * output_width + true_grid_x ]

            coord_mask[ target_index ][ best_index_anchor ][ true_grid_y ][ true_grid_x ]           = 1
            cls_mask[ target_index ][ best_index_anchor ][ true_grid_y ][ true_grid_x ]             = 1
            conf_no_object_mask[ target_index ][ best_index_anchor ][ true_grid_y ][ true_grid_x ]  = object_scale
            tx[ target_index ][ best_index_anchor ][ true_grid_y ][ true_grid_x ]                   = target[ target_index ][ t * 5 + 1 ] * output_width - true_grid_x
            ty[ target_index ][ best_index_anchor ][ true_grid_y ][ true_grid_x ]                   = target[ b ][ t * 5 + 2 ] * output_height - true_grid_y
            tw[ target_index ][ best_index_anchor ][ true_grid_y ][ true_grid_x ]                   = math.log( true_width / anchors[ anchor_step * best_index_anchor ] )
            th[ target_index ][ best_index_anchor ][ true_grid_y ][ true_grid_x ]                   = math.log( true_height / anchors[ anchor_step * best_index_anchor + 1 ] )

            iou = iou_single_boxes( true_box , pred_box , is_centroid = True )

            tconf[ target_index ][ best_index_anchor ][ true_grid_y ][ true_grid_x ] = iou
            tcls[ target_index ][ best_index_anchor ][ true_grid_y ][ true_grid_x ]  = target[ target_index ][ t * 5 ]

            if iou > 0.5:
                count_correct = count_correct + 1

    return count_true_boxes, count_correct , coord_mask , conf_no_object_mask , cls_mask , tx , ty , tw , th , tconf , tcls

class RegionLoss(nn.Module):
    def __init__(self, num_classes=0, anchors=[], num_anchors=1):
        super(RegionLoss, self).__init__()
        self.num_classes = num_classes
        self.anchors = anchors
        self.num_anchors = num_anchors
        self.anchor_step = len(anchors)/num_anchors
        self.coord_scale = 1
        self.noobject_scale = 1
        self.object_scale = 5
        self.class_scale = 1
        self.thresh = 0.6

    def forward(self, output, target):
        num_anchors   = self.num_anchors
        num_classes   = self.num_classes
        output_count  = output.data.size(0)
        output_height = output.data.size(2)
        output_width  = output.data.size(3)
        
        if has_cuda:
            LongTensorType = torch.cuda.LongTensor
            FloatTensorType = torch.cuda.LongTensor
        else:
            LongTensorType = torch.LongTensor
            FloatTensorType = torch.FloatTensor

        output   = output.view( output_count , num_anchors , ( 5 + num_classes ), output_height , output_width )
        
        x    = F.sigmoid( output.index_select( 2 , Variable( LongTensorType( [ 0 ] ) ) ).view( output_count , num_anchors , output_height , output_width ) )
        y    = F.sigmoid( output.index_select( 2 , Variable( LongTensorType( [ 1 ] ) ) ).view( output_count , num_anchors , output_height , output_width ) )
        w    = output.index_select( 2 , Variable( LongTensorType( [ 2 ] ) ) ).view( output_count , num_anchors , output_height , output_width )
        h    = output.index_select( 2 , Variable( LongTensorType( [ 3 ] ) ) ).view( output_count , num_anchors , output_height , output_width )
        conf = F.sigmoid( output.index_select( 2 , Variable( LongTensorType( [ 4 ] ) ) ).view( output_count , num_anchors , output_height , output_width ) )
        cls  = torch.linspace( 5 ,  5 + num_classes - 1 , num_classes ).long()
        if has_cuda:
            cls = cls.cuda()
        cls  = output.index_select( 2 , Variable( cls ) ) 
        cls  = cls.view( output_count * num_anchors , num_classes , output_height * output_width ).transpose( 1 , 2 ).contiguous()
        cls  = cls.view( output_count * num_anchors * output_height * output_width , num_classes )

        pred_boxes = FloatTensorType( 4 , num_anchors * output_count * output_width * output_height )
        grid_x = torch.linspace( 0 , output_width - 1 , output_width ).repeat( output_height , 1 ).repeat( num_anchors * output_count , 1 , 1 ).view( num_anchors * output_count * output_height * output_width )
        grid_y = torch.linspace( 0 , output_height - 1 , output_height ).repeat( output_width , 1 ).t().repeat( num_anchors * output_count , 1 , 1 ).view( num_anchors * output_count * output_height * output_width )
        anchor_w = torch.Tensor( self.anchors ).view( num_anchors , self.anchor_step ).index_select( 1 , torch.LongTensor( [ 0 ] ) ) 
        anchor_h = torch.Tensor( self.anchors ).view( num_anchors , self.anchor_step ).index_select( 1 , torch.LongTensor( [ 1 ] ) ) 
        anchor_w = anchor_w.repeat( output_count , 1 ).repeat( 1 , 1 , output_height * output_width ).view( num_anchors * output_count * output_height * output_height )
        anchor_h = anchor_h.repeat( output_count , 1 ).repeat( 1 , 1 , output_height * output_width ).view( num_anchors * output_count * output_height * output_height )
        if has_cuda:
            grid_x = grid_x.cuda()
            grid_y = grid_x.cuda()
            anchor_w = grid_x.cuda()
            anchor_h = grid_x.cuda()

        pred_boxes[ 0 ] = x.data + grid_x.view( x.size() )
        pred_boxes[ 1 ] = y.data + grid_y.view( y.size() )
        pred_boxes[ 2 ] = torch.exp( w.data ) * anchor_w.view( w.size() )
        pred_boxes[ 3 ] = torch.exp( h.data ) * anchor_h.view( y.size() )
        pred_boxes = convert_to_cpu( pred_boxes.transpose( 0 , 1 ).contiguous().view( -1 , 4 ) )

        count_true_boxes , count_correct , coord_mask , conf_no_object_mask , cls_mask , tx , ty , tw , th , tconf ,tcls = build_targets( pred_boxes , target.data , self.anchors , num_anchors , num_classes , \
                                                                                                                                          output_height , output_width , self.noobject_scale , self.object_scale , self.thresh )
        cls_mask = ( cls_mask == 1 )
        count_proposals = int( ( conf > 0.25 ).sum().data[ 0 ] )
        
        if has_cuda:
            tx                  = tx.cuda()
            ty                  = ty.cuda()
            tw                  = tw.cuda()
            th                  = th.cuda()
            tconf               = tconf.cuda()
            tcls                = tcls.cuda()
            coord_mask          = coord_mask.cuda()
            conf_no_object_mask = conf_no_object_mask.cuda()
            cls_mask            = cls_mask.cuda()

        tx    = Variable( tx )
        ty    = Variable( ty )
        tw    = Variable( tw )
        th    = Variable( th )
        tconf = Variable( tconf )
        tcls  = Variable( tcls.view( -1 )[ cls_mask ].long() )

        coord_mask          = Variable( coord_mask )
        conf_no_object_mask = Variable( conf_no_object_mask.sqrt() )
        cls_mask            = Variable( cls_mask.view( -1 , 1 ).repeat( 1 , num_classes ) )
        cls                 = cls[ cls_mask ].view( -1 , num_classes )  

        loss_x = self.coord_scale * nn.MSELoss( size_average = False )( x * coord_mask , tx * coord_mask ) / 2.0
        loss_y = self.coord_scale * nn.MSELoss( size_average = False )( y * coord_mask , ty * coord_mask ) / 2.0
        loss_w = self.coord_scale * nn.MSELoss( size_average = False )( w * coord_mask , tw * coord_mask ) / 2.0
        loss_h = self.coord_scale * nn.MSELoss( size_average = False )( h * coord_mask , th * coord_mask ) / 2.0
        loss_conf = nn.MSELoss( size_average = False )( conf * conf_no_object_mask , tconf * conf_no_object_mask ) / 2.0
        loss_cls = self.class_scale * nn.CrossEntropyLoss( size_average = False )( cls , tcls )
        loss = loss_x + loss_y + loss_w + loss_h + loss_conf + loss_cls
        return loss
