import cv2

def detect( args ):
    cap = cv2.VideoCapture( int( args.path ) )
    if not cap.isOpened():
        args.parser.print_help()
        args.parser.error( "Couldn't connect with camera" )
        return

    #from Yolo import Darknet
    #model = Darknet( None , args )
    while( cap.isOpened() ):
        ret, img_bytes = cap.read()
        if ret == False:
            print "Error reading from camera"
            break

        cv2.imshow( 'frame' ,img_bytes )
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
