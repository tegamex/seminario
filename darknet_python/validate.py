import os
import xml.etree.ElementTree as ET
classess_names = [ [ "person" ] , [ "bird" , "cat" , "cow" , "dog" , "horse" , "sheep" ] ]
class_name = [ "person" , "animal" ]
file_name = "model_raspberry"
type_data = "valid"

def parse_key_file( path ):
    result = dict()
    try:
        with open( path , 'r') as fp:
            lines = fp.readlines()
            for line in lines:
                line = line.strip()
                if line == '':
                    continue
                key , value = line.split( '=' )
                key = key.strip()
                value = value.strip()
                result[ key ] = value
            fp.close()
    except IOError:
        logging( 'Error reading file %s' % ( path ) )
    return result
from bk import db
# ( x1 , y1 , x2 , y2 ) ( x1 , y1 , x2 , y2 )
def get_iou( box1 , box2 ):
    min_x    = min( box1[ 0 ] , box2[ 0 ] )
    max_x    = max( box1[ 2 ] , box2[ 2 ] )
    min_y    = min( box1[ 1 ] , box2[ 1 ] )
    max_y    = max( box1[ 3 ] , box2[ 3 ] )
    width_1  = box1[ 2 ] - box1[ 0 ]
    width_2  = box2[ 2 ] - box2[ 0 ]
    height_1 = box1[ 3 ] - box1[ 1 ]
    height_2 = box2[ 3 ] - box2[ 1 ]
    union_width                               = max_x - min_x
    union_height                              = max_y - min_y
    intersection_width                        = width_1 + width_2 - union_width
    intersection_height                       = height_1 + height_2 - union_height
    if intersection_width <= 0 or intersection_height <= 0:
        return 0.0
    area_1                                    = width_1 * height_1
    area_2                                    = width_2 * height_2
    intersection_area                         = intersection_width * intersection_height
    union_area                                = area_1 + area_2 - intersection_area
    return intersection_area / union_area
    
def analyze( boxes , truth_boxes ):
    error = 0
    total = 0 
    for truth_box in truth_boxes:
        IOU = 0
        total = total + 1
        for box in boxes:
            temp_iou = get_iou( box , truth_box )
            if IOU < temp_iou:
                IOU = temp_iou
        error = error + 1 - IOU
    return error , total
    
def get_size( file_name ):
    file_name = file_name.replace( "labels" , "Annotations" ).replace( "txt" , "xml" )
    tree = ET.parse( file_name )
    root_node = tree.getroot()
    size_node = root_node.find( 'size' )
    width = int( size_node.find( 'width' ).text )
    height = int( size_node.find( 'height' ).text )
    return ( width , height )
    
def check( index_class , file_name , boxes ):
    file_name = file_name.replace( "kpruiz11" , "kevin" ).replace( "seminario" , "researches" ).replace( "JPEGImages" , "labels" ).replace( "jpg" , "txt" ).rstrip( "\n" )
    ( w , h ) = get_size( file_name )
    error = 0
    total = 0
    with open( file_name ) as fp:
        truth_boxes = []
        for line in fp:
            items = line.rstrip( "\n" ).split( " " )
            if int( items[ 0 ] ) != index_class:
                continue
            box = [ float( pos ) for pos in items[ 1 : 5 ] ]
            box = [ box[ 0 ] - box[ 2 ] / 2. , box[ 1 ] - box[ 3 ] / 2. , box[ 0 ] + box[ 2 ] / 2. , box[ 1 ] + box[ 3 ] / 2. ]
            box = [ box[ 0 ] * w , box[ 1 ] * h , box[ 2 ] * w , box[ 3 ] * h ]
            truth_boxes.append( box )
        _error , _total = analyze( boxes , truth_boxes )
        error = error + _error
        total = total + _total
    return error , total
    
def run( args ):
    valid_config = parse_key_file( args.config )
    error_total = 0
    total_total = 0
    for i in range( len( class_name ) ):
        valid_list = open( valid_config[ type_data ] )
        valid_output = open( os.path.join( valid_config[ "results" ] , "%s_%s.txt" % ( file_name , class_name[ i ] ) ) )
        current_line = ""
        error = 0
        total = 0
        for line in valid_list:
            file_id = os.path.basename( line ).split( "." )[ 0 ]
            boxes = []
            cont = True
            if current_line != "":
                temp = current_line.split( " " )
                if temp[ 0 ] != file_id :
                    cont = False
                else:
                    box = [ float( item ) for item in temp[ 2 : 6 ] ]
                    boxes.append( box )
            while cont:
                current_line = valid_output.readline()
                if current_line is None:
                    break
                temp = current_line.split( " " )
                if temp[ 0 ] != file_id :
                    break
                box = [ float( item ) for item in temp[ 2 : 6 ] ]
                boxes.append( box )
            _error , _total = check( i , line , boxes )
            error = error + _error
            total = total + _total 
        error_total = error_total + error
        total_total = total_total + total
        print "El Error total en IOU para clase %s , %s " % ( class_name[ i ] , error / total ) 
    print "El Error total en IOU general es %s " % ( error_total / total_total )         
    
