from bk import db
import torch.optim 
import torch.utils.data
from torchvision import transforms
from dataset_loader import DataSetLoader
from convolutional_network import ConvNet
from utils import *

#def adjust_learning_rate( optimizer ):
#    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
#    lr = learningRate
#    for i in range( len( steps ) ):
#        scale = scales[ i ] if i < len( scales ) else 1
#        if batch >= steps[ i ]:
#            lr = lr * scale
#            if batch == steps[ i ]:
#                break
#        else:
#            break
#    for param_group in optimizer.param_groups:
#        param_group[ 'lr' ] = lr / batchSize
#    return lr

def run( args ):

    if args.model is None or not os.path.isfile( args.model ):
        args.parser.print_help()
        args.parser.error( "Error path of file of model" )

    if args.config is None or not os.path.isfile( args.config ):
        args.parser.print_help()
        args.parser.error( "Insert path of config file( operation 'generate if you don't have one)" )

    data_config = parse_key_file( args.config )
    model_file = parse_model_file( args.model )
    assert model_file[ 0 ][ "type" ] == "net" , "Error in model file"
    assert model_file[ -1 ][ "type" ] in { "cost" , "region" } , "Error in model_file"
    assert os.path.isfile( data_config[ 'list' ] ) , 'Error in data file'
    assert os.path.isdir( data_config[ 'images' ] ) , 'Error in data file'
    assert os.path.isdir( data_config[ 'labels' ] ) , 'Error in data file'

    network_option = model_file[ 0 ] 

    batch_size      = int( network_option[ 'batch' ] )
    max_batches     = int( network_option[ 'max_batches' ] )
    learning_rate   = float( network_option[ 'learning_rate' ] )
    momentum        = float( network_option[ 'momentum' ] )
    decay           = float( network_option[ 'decay' ] )
    width           = int( network_option[ 'width' ] )
    height          = int( network_option[ 'height' ] )
    #steps          = [ float( step ) for step in network_option[ 'steps' ].split( ',' ) ]
    #scales         = [ float( scale ) for scale in network_option[ 'scales' ].split( ',' ) ]

    function_loss = create_model_cost( model_file[ -1 ] ) 
    
    list_ids  = parse_line_file( data_config[ 'list' ] )
    random.shuffle( list_ids )
    train_ids = list_ids[ 0 : 2 * len( list_ids ) / 3 ]
    test_ids  = list_ids[ 2 * len( list_ids ) / 3 : len( list_ids ) ]

    dataset_train_loader = DataSetLoader( train_ids , data_config[ 'images' ] , data_config[ 'labels' ] , size = ( width , height ) , transform = transforms.Compose( [ transforms.ToTensor() , ] ) , train = True )
    train_loader         = torch.utils.data.DataLoader( dataset_train_loader , batch_size = batch_size , pin_memory = args.cuda , shuffle = True )

    dataset_test_loader  = DataSetLoader( test_ids , data_config[ 'images' ] , data_config[ 'labels' ] , size = ( width , height ) , transform = transforms.Compose( [ transforms.ToTensor() , ] ) , train = False )
    test_loader          = torch.utils.data.DataLoader( dataset_test_loader , batch_size = batch_size , pin_memory = args.cuda , shuffle = True )

    model = ConvNet( args )
    if args.cuda:
        model.cuda()

    optimizer = torch.optim.SGD( model.parameters() ,
                           lr = learning_rate / batch_size,
                           momentum = momentum ,
                           dampening = 0 ,
                           weight_decay = decay * batch_size )

    epochs = int( args.epochs )
    for i in range( epochs ):
        #adjust_learning_rate( optimizer )
        model.train()
        for _ , ( X , Y ) in enumerate( train_loader ):

            if args.cuda:
                X = X.cuda()
                Y = Y.cuda()
            else:
                X = X[ 0 ].unsqueeze( 0 )
                Y = Y[ 0 ].unsqueeze( 0 )
        
            X , Y = Variable( X ), Variable( Y )
            optimizer.zero_grad()
            Y_pred = model( X )
            loss = function_loss( Y_pred , Y )
            db()
            loss_value = loss.data.numpy()[ 0 ]
            loss.backward()
            optimizer.step()
            print 'Current loss %s' % ( loss_value )
        db()
        model.eval()
        for _ , ( X , Y ) in enumerate( test_loader ):
            X = X[ 0 ].unsqueeze( 0 )
            Y = Y[ 0 ].unsqueeze( 0 )
            if args.cuda:
                X = X.cuda()
                Y = Y.cuda()
            X , Y = Variable( X ), Variable( Y )
            Y_pred = model( X )
            loss = function_loss( Y_pred , Y )
        logging( 'Finished epoch %s' % ( i + 1 ) )
    db()
