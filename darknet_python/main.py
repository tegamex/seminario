import argparse
def main():
    parser = argparse.ArgumentParser( formatter_class = argparse.RawTextHelpFormatter )
    parser.add_argument( "-o" , "--operation" , dest = "operation" , help = "Operation to realize( generate , train , test , detect , validate )" , default = "generate" )
    parser.add_argument( "-p" , "--path" , dest = "path" , help =
                        "In generate\n"+
                            "\tdirectory of Pascal VOC\n"+
                        "In training\n"+
                            "\tpath of data to train.\n"+
                        "In testing\n"+
                            "\path of data to test.\n"+
                        "In detection\n"
                            "\tpath of file to test,\n"+
                            "\ta positive number to connect with camera,\n" , 
                        default = "." )
    parser.add_argument( "-m" , "--model" , dest = "model" , help = "path of model config" )
    parser.add_argument( "-w" , "--weights" , dest = "weights" , help = "path of weights file" )
    parser.add_argument( "-e" , "--epochs" , dest = "epochs" , help = "In training\n"+
                                                                          "\tnumber or iterations",
                        default = "200" )
    parser.add_argument( "-c" , "--config" , dest = "config" , help = "Config of data" )
    parser.add_argument( "-t" , "--thresh" , dest = "thresh" , help = "Thresh of detections" , default = "0.5" )
    parser.add_argument( "-out" , "--output" , dest = "output" , help = "Path of output generated for such operation" , default = "." )
    args = parser.parse_args()
    setattr( args , "parser" , parser )
    if args.operation == "train":
        from train import run
    elif args.operation == "generate":
        from generate import run
    elif args.operation == "detect":
        from detect import run
    elif args.operation == "validate":
        from validate import run
    else:
        parser.print_help()
        parser.error( "Operation no valid" )
    run( args )

if __name__ == "__main__":
    main()
