from bk import db
import os
import random
import xml.etree.ElementTree as ET

# 0.3 to validation , 0.7 to training
valid_proportion = 0.3
years =[ '2012' ]
classes_1 = { "person" }
classes_2 = { "bird" , "cat" , "cow" , "dog" , "horse" , "sheep" }
classes_3 = { "aeroplane" , "bicycle" , "boat" , "bottle" , "bus" , "car" , "chair" , "diningtable" , "motorbike" , "pottedplant" , "sofa" , "train" , "tvmonitor" }

def get_proportions( width , height , box ):
    dw = 1./width
    dh = 1./height
    x = ( box[ 0 ] + box[ 1 ] ) / 2.
    y = ( box[ 2 ] + box[ 3 ] ) / 2.
    w = box[ 1 ] - box[ 0 ]
    h = box[ 3 ] - box[ 2 ]
    x = x * dw
    y = y * dh
    w = w * dw
    h = h * dh
    return ( x , y , w , h )

def clear_directory( path ):
    for file_item in os.listdir( path ):
        file_path = os.path.join( path , file_item )
        try:
            if os.path.isfile( file_path ):
                os.unlink( file_path )
        except Exception as e:
            print(e)

def write_annotation( path , item ):

    input_file = open( '%s/Annotations/%s.xml'%( path , item ) )
    out_file = open( '%s/labels/%s.txt' % ( path, item ), 'w' )

    tree = ET.parse( input_file )
    root_node = tree.getroot()
    size_node = root_node.find( 'size' )
    width = int( size_node.find( 'width' ).text )
    height = int( size_node.find( 'height' ).text )
        
    result = False
    for obj in root_node.iter( 'object' ):
        
        class_name = obj.find( 'name' ).text
        if class_name in classes_1:
            class_id = 0
        elif class_name in classes_2:
            class_id =1 
        else:
            continue
        xmlbox = obj.find( 'bndbox' )
        box = ( float( xmlbox.find( 'xmin' ).text ) , float( xmlbox.find( 'xmax' ).text ) , float( xmlbox.find( 'ymin' ).text ), float( xmlbox.find( 'ymax' ).text ) )
        proportions = get_proportions( width , height , box )
        if proportions[ 2 ] < 0.001 or proportions[ 3 ] < 0.001:
            continue
        result = True     
        out_file.write( str( class_id ) + " " + " ".join( [ str( a ) for a in proportions ] ) + '\n')
        
    return result

def run( params ):
    train_list = open( 'train.txt' , 'w' )
    valid_list = open( 'val.txt' , 'w' )
    for year in years:
        if not os.path.isdir( '%s/VOCdevkit/VOC%s/Annotations'%( params.path , year ) ):
            params.parser.print_help()
            params.parser.error( 'Error in path of VOC data')

        list_files = os.listdir( '%s/VOCdevkit/VOC%s/Annotations'%( params.path , year ) )
        list_files = [ item.split( '.' )[ 0 ] for item  in list_files ]
    
        for item in list_files :
            result = write_annotation( os.path.join( params.path , 'VOCdevkit/VOC%s' % ( year ) ) , item )
            if not result :
                continue
            x = random.uniform( 0 , 1 )
            if x < valid_proportion:
                valid_list.write( os.path.join( os.path.abspath( params.path ) , 'VOCdevkit/VOC%s/JPEGImages/%s.jpg' %( year , item )  ) + '\n' )
            else:
                train_list.write( os.path.join( os.path.abspath( params.path ) , 'VOCdevkit/VOC%s/JPEGImages/%s.jpg' %( year , item )  ) + '\n' )
