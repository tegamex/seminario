import sys
import os
import time
import math
import torch
import random
import numpy as np
from PIL import Image, ImageDraw, ImageFont
from torch.autograd import Variable

import struct # get_image_size
import imghdr # get_image_size
from bk import db

def sigmoid(x):
    return 1.0/(math.exp(-x)+1.)

def softmax(x):
    x = torch.exp(x - torch.max(x))
    x = x/x.sum()
    return x

def from_image_to_torch( img ):
    width = img.width
    height = img.height
    img = torch.ByteTensor( torch.ByteStorage.from_buffer( img.tobytes() ) )
    img = img.view( height , width , 3 ).transpose( 0 , 1 ).transpose( 0 , 2 ).contiguous()
    img = img.view( 1 , 3 , height , width )
    img = img.float().div( 255.0 )
    return img

def from_numpy_to_torch( img ):
    return torch.from_numpy( img.transpose( 2 , 0 , 1 ) ).float().div( 255.0 ).unsqueeze( 0 )

def convert_to_cpu( matrix ):
    return torch.FloatTensor( matrix.size() ).copy_( matrix )

def convert_to_cpu_long( matrix ):
    return torch.LongTensor( matrix.size() ).copy_( matrix )

def iou_single_boxes( box1 , box2 , is_centroid = False ):
    if is_centroid:
        min_x    = min( box1[ 0 ] - box1[ 2 ] / 2.0 , box2[ 0 ] - box2[ 2 ] / 2.0 )
        max_x    = max( box1[ 0 ] + box1[ 2 ] / 2.0 , box2[ 0 ] + box2[ 2 ] / 2.0 )
        min_y    = min( box1[ 1 ] - box1[ 3 ] / 2.0 , box2[ 1 ] - box2[ 3 ] / 2.0 )
        max_y    = max( box1[ 1 ] + box1[ 3 ] / 2.0 , box2[ 1 ] + box2[ 3 ] / 2.0 )
        width_1   = box1[ 2 ]
        width_2  = box2[ 2 ]
        height_1 = box1[ 3 ]
        height_2 = box2[ 3 ]
    else:
        min_x    = min( box1[ 0 ] , box2[ 0 ] )
        max_x    = max( box1[ 2 ] , box2[ 2 ] )
        min_y    = min( box1[ 1 ] , box2[ 1 ] )
        max_y    = max( box1[ 3 ] , box2[ 3 ] )
        width_1  = box1[ 2 ] - box1[ 0 ]
        width_2  = box2[ 2 ] - box2[ 0 ]
        height_1 = box1[ 3 ] - box1[ 1 ]
        height_2 = box2[ 3 ] - box2[ 1 ]
    union_width                               = max_x - min_x
    union_height                              = max_y - min_y
    intersection_width                        = width_1 + width_2 - union_width
    intersection_height                       = height_1 + height_2 - union_height
    if intersection_width <= 0 or intersection_height <= 0:
        return 0.0
    area_1                                    = width_1 * height_1
    area_2                                    = width_2 * height_2
    intersection_area                         = intersection_width * intersection_height
    union_area                                = area_1 + area_2 - intersection_area
    return intersection_area / union_area

def iou_multiple_boxes( boxes1 , boxes2 , is_centroid = False ):
    if is_centroid:
        min_x    = torch.min( boxes1[ 0 ] - boxes1[ 2 ] / 2.0 , boxes2[ 0 ] - boxes2[ 2 ] / 2.0 )
        max_x    = torch.max( boxes1[ 0 ] + boxes1[ 2 ] / 2.0 , boxes2[ 0 ] + boxes2[ 2 ] / 2.0 )
        min_y    = torch.min( boxes1[ 1 ] - boxes1[ 3 ] / 2.0 , boxes2[ 1 ] - boxes2[ 3 ] / 2.0 )
        max_y    = torch.max( boxes1[ 1 ] + boxes1[ 3 ] / 2.0 , boxes2[ 1 ] + boxes2[ 3 ] / 2.0 )
        width_1  = boxes1[ 2 ]
        width_2  = boxes2[ 2 ]
        height_1 = boxes1[ 3 ]
        height_2 = boxes2[ 3 ]
    else:
        min_x    = torch.min( boxes1[ 0 ] , boxes2[ 0 ] )
        max_x    = torch.max( boxes1[ 2 ] , boxes2[ 2 ] )
        min_y    = torch.min( boxes1[ 1 ] , boxes2[ 1 ] )
        max_y    = torch.max( boxes1[ 3 ] , boxes2[ 3 ] )
        width_1  = boxes1[ 2 ] - boxes1[ 0 ]
        width_2  = boxes2[ 2 ] - boxes2[ 0 ]
        height_1 = boxes1[ 3 ] - boxes1[ 1 ]
        height_2 = boxes2[ 3 ] - boxes2[ 1 ]

    union_width                               = max_x - min_x
    union_height                              = max_y - min_y
    intersection_width                        = width_1 + width_2 - union_width
    intersection_height                       = height_1 + height_2 - union_height
    mask_no_intersection                      = ( ( intersection_width <= 0 ) + ( intersection_height <= 0 ) > 0 )
    area_1                                    = width_1 * height_1
    area_2                                    = width_2 * height_2
    intersection_area                         = intersection_width * intersection_height
    intersection_area[ mask_no_intersection ] = 0
    union_area                                = area_1 + area_2 - intersection_area
    return intersection_area / union_area

def nms_filter( boxes , nms_thresh ):
    if len( boxes ) == 0:
        return boxes

    det_confs = torch.zeros( len( boxes ) )

    for i in range( len( boxes ) ):
        det_confs[ i ] = 1 - boxes[ i ][ 4 ]                

    _ , sort_ids = torch.sort( det_confs )
    out_boxes = []
    for i in range( len ( boxes ) ):
        box_i = boxes[ sort_ids [ i ] ]
        if box_i[ 4 ] > 0:
            out_boxes.append( box_i )
            for j in range( i + 1 , len( boxes ) ):
                box_j = boxes[ sort_ids[ j ] ]
                if iou_single_boxes( box_i , box_j , is_centroid = True ) > nms_thresh:
                    box_j[ 4 ] = 0
    return out_boxes

def filter_boxes( output , data_conf , validation = False ):

    anchors     = data_conf[ "anchors" ]
    num_anchors = data_conf[ "num_anchors" ]
    num_classes = data_conf[ "num_classes" ]
    conf_thresh = data_conf[ "conf_thresh" ]
    only_objectness = "names" in data_conf
    
    anchor_step = len( anchors ) / num_anchors

    if output.dim() == 3:
        output = output.unsqueeze( 0 )

    batch_size  = output.size( 0 )
    assert( output.size( 1 ) == ( 5 + num_classes ) * num_anchors )

    height = output.size( 2 )
    width  = output.size( 3 )

    output = output.view( batch_size * num_anchors , 5 + num_classes, height * width ).transpose( 0 , 1 ).contiguous().view( 5 + num_classes , batch_size * num_anchors * height * width )

    grid_x = torch.linspace( 0 , width - 1 , width ).repeat( height , 1 ).repeat( batch_size * num_anchors , 1 , 1 ).view( batch_size * num_anchors * height * width )
    grid_y = torch.linspace( 0 , height - 1 , height ).repeat( width , 1 ).t().repeat( batch_size * num_anchors , 1 , 1 ).view( batch_size * num_anchors * height * width )

    xs = torch.sigmoid( output[ 0 ] ) + grid_x
    ys = torch.sigmoid( output[ 1 ] ) + grid_y

    anchor_w = torch.Tensor( anchors ).view( num_anchors , anchor_step ).index_select( 1 , torch.LongTensor( [ 0 ] ) )
    anchor_h = torch.Tensor( anchors ).view( num_anchors , anchor_step ).index_select( 1 , torch.LongTensor( [ 1 ] ) )

    anchor_w = anchor_w.repeat( batch_size , 1 ).repeat( 1 , 1 , height * width ).view( batch_size * num_anchors * height * width )
    anchor_h = anchor_h.repeat( batch_size , 1 ).repeat( 1 , 1 , height * width ).view( batch_size * num_anchors * height * width )

    ws = torch.exp( output[ 2 ] ) * anchor_w
    hs = torch.exp( output[ 3 ] ) * anchor_h

    det_confs = torch.sigmoid( output[ 4 ] )

    cls_confs = torch.nn.Softmax()( Variable( output[ 5 : 5 + num_classes ].transpose( 0 , 1 ) ) ).data

    cls_max_confs , cls_max_ids = torch.max( cls_confs , 1 )
    cls_max_confs = cls_max_confs.view( -1 )
    cls_max_ids   = cls_max_ids.view( -1 )
    
    size         = height * width
    size_anchors = size * num_anchors
    detConfs     = convert_to_cpu( det_confs )
    clsMaxConfs  = convert_to_cpu( cls_max_confs )
    cls_max_ids  = convert_to_cpu_long( cls_max_ids )

    xs = convert_to_cpu( xs )
    ys = convert_to_cpu( ys )
    ws = convert_to_cpu( ws )
    hs = convert_to_cpu( hs )

    if validation:
        cls_confs = convert_to_cpu( cls_confs.view( -1 , num_classes ) )

    all_boxes = []
    for b in range( batch_size ):
        boxes = []
        for cy in range( height ):
            for cx in range( width ):
                for i in range( num_anchors ):
                    ind = b * size_anchors + i * size + cy * width + cx
                    det_conf =  det_confs[ ind ]
                    if only_objectness:
                        conf =  det_confs[ ind ]
                    else:
                        conf = det_confs[ ind ] * cls_max_confs[ ind ]
    
                    if conf < conf_thresh:
                        continue
                    bcx = xs[ ind ]
                    bcy = ys[ ind ]
                    bw = ws[ ind ]
                    bh = hs[ ind ]
                    cls_max_conf = cls_max_confs[ ind ]
                    cls_max_id = cls_max_ids[ ind ]
                    box = [ bcx / width , bcy / height , bw / width , bh / height, det_conf, cls_max_conf, cls_max_id ]
                    if ( not only_objectness ) and validation:
                        for c in range( num_classes ):
                            tmp_conf = cls_confs[ ind ][ c ]
                            if c != cls_max_id and det_confs[ ind ] * tmp_conf > conf_thresh:
                                box.append( tmp_conf ) 
                                box.append( c )
                    boxes.append( box )
        all_boxes.append( boxes )
    return all_boxes

def get_rand_color( c , x , maxVal ):
    colors = torch.FloatTensor( [ [ 1 , 0 , 1 ] , [ 0 , 0 , 1 ] , [ 0 , 1 , 1 ] , [ 0 , 1 , 0 ] , [ 1 , 1 , 0 ] , [ 1 , 0 , 0 ] ] )
    ratio = float( x ) / maxVal * 5
    i = int( math.floor( ratio ) )
    j = int( math.ceil( ratio ) )
    ratio = ratio - i
    r = ( 1 - ratio ) * colors[ i ][ c ] + ratio * colors[ j ][ c ]
    return int( r * 255 )

def draw_results( img_data , boxes , name_classes = None ):
    import cv2
    if type( img_data ) == torch.autograd.Variable :
        if img_data.size( 0 ) == 3:
            img_data = img_data.data.numpy().transpose( 1 , 2 , 0 ).dot( 255 ).astype( np.uint8 ).copy()
        else:
            assert False , "Error in data to print resuts" 
    else:
        assert False , "Error in data to print resuts" 

    width = img_data.shape[ 0 ]
    height = img_data.shape[ 1 ]

    if name_classes:
        num_classes = len( name_classes )
    else:
        num_classes = 20

    for i in range( len( boxes ) ):
        box = boxes[ i ]
        x1 = int( ( box[ 0 ] - box[ 2 ] / 2.0 ) * width )
        y1 = int( ( box[ 1 ] - box[ 3 ] / 2.0 ) * height )
        x2 = int( ( box[ 0 ] + box[ 2 ] / 2.0 ) * width )
        y2 = int( ( box[ 1 ] + box[ 3 ] / 2.0 ) * height )

        rgb = ( 255 , 0 , 0 )
        assert( len( box ) >= 7 )
        cls_conf = box[ 5 ]
        cls_id   = box[ 6 ]
        offset   = cls_id * 123457 % num_classes
        red      = get_rand_color( 2 , offset , num_classes )
        green    = get_rand_color( 1 , offset , num_classes )
        blue     = get_rand_color( 0 , offset , num_classes )
        rgb = ( red , green, blue )

        if name_classes :
            img_data = cv2.putText( img_data , name_classes[ cls_id ] , ( x1 , y1 ) , cv2.FONT_HERSHEY_SIMPLEX , 1.2 , rgb , 1 )
        img_data = cv2.rectangle( img_data , ( x1 , y1 ) , ( x2 , y2 ) , rgb , 1 )
    return img_data

def detect_from_image( model , img_data , data_conf , conf_thresh = 0.5 , nms_thresh = 0.4 ):

    if isinstance( img_data , Image.Image ):
        img_data = from_image_to_torch( img )
    elif type( img_data ) == np.ndarray: 
        img_data = from_numpy_to_torch( img_data )
    else:
        print("unknow image type")
        exit( -1 )

    img_data = torch.autograd.Variable( img_data )

    output = model( img_data )
    output = output.data
    output = filter_boxes( output , data_conf )

    boxes = output[ 0 ]

    boxes = nms_filter( boxes , data_conf[ "nms_thresh" ] )
    result = draw_results( img_data[ 0 ] , boxes , data_conf[ "names" ] if "names" in data_conf else None )

    return result

def scale_bboxes(bboxes, width, height):
    import copy
    dets = copy.deepcopy(bboxes)
    for i in range(len(dets)):
        dets[i][0] = dets[i][0] * width
        dets[i][1] = dets[i][1] * height
        dets[i][2] = dets[i][2] * width
        dets[i][3] = dets[i][3] * height
    return dets

def get_image_size(fname):
    '''Determine the image type of fhandle and return its size.
    from draco'''
    with open(fname, 'rb') as fhandle:
        head = fhandle.read(24)
        if len(head) != 24: 
            return
        if imghdr.what(fname) == 'png':
            check = struct.unpack('>i', head[4:8])[0]
            if check != 0x0d0a1a0a:
                return
            width, height = struct.unpack('>ii', head[16:24])
        elif imghdr.what(fname) == 'gif':
            width, height = struct.unpack('<HH', head[6:10])
        elif imghdr.what(fname) == 'jpeg' or imghdr.what(fname) == 'jpg':
            try:
                fhandle.seek(0) # Read 0xff next
                size = 2 
                ftype = 0 
                while not 0xc0 <= ftype <= 0xcf:
                    fhandle.seek(size, 1)
                    byte = fhandle.read(1)
                    while ord(byte) == 0xff:
                        byte = fhandle.read(1)
                    ftype = ord(byte)
                    size = struct.unpack('>H', fhandle.read(2))[0] - 2 
                # We are at a SOFn block
                fhandle.seek(1, 1)  # Skip `precision' byte.
                height, width = struct.unpack('>HH', fhandle.read(4))
            except Exception: #IGNORE:W0703
                return
        else:
            return
        return width, height

def logging( message ):
    print( '%s %s' % (time.strftime( "%Y-%m-%d %H:%M:%S" , time.localtime() ) , message ) )

def create_model_cost( block ):
    model = None
    if block[ 'type' ] == 'region':
        from region_loss import RegionLoss
        model = RegionLoss()
        anchors = block[ 'anchors' ].split( ',' )
        model.anchors = [float(i) for i in anchors]
        model.num_classes = int( block['classes'] )
        model.num_anchors = int( block[ 'num' ] )
        model.anchor_step = len( model.anchors ) / model.num_anchors
        model.object_scale = float( block[ 'object_scale' ] )
        model.noobject_scale = float( block[ 'noobject_scale' ] )
        model.class_scale = float( block[ 'class_scale' ] )
        model.coord_scale = float( block[ 'coord_scale' ] )

    elif block['type'] == 'cost':
        if block[ '_type' ] == 'sse':
            model = nn.MSELoss( size_average = True )

        elif block[ '_type' ] == 'L1':
            model = nn.L1Loss( size_average = True )

        elif block[ '_type' ] == 'smooth':
            model = nn.SmoothL1Loss( size_average = True )

        else:
            assert False , "Type of cost function no implemented"

    else:
        assert False , "Type of loss function no implemented"

    return model

def parse_line_file( path ):
    result = []
    try:
        with open( path , 'r' ) as fp:
            lines = fp.readlines()
            for line in lines:
                line = line.rstrip()
                if line == '' :
                    continue        
                result.append( line )
            fp.close()
    except IOError:
        logging( 'Error reading file %s' % ( path ) )
    return result

def parse_key_file( path ):
    result = dict()
    try:
        with open( path , 'r') as fp:
            lines = fp.readlines()
            for line in lines:
                line = line.strip()
                if line == '':
                    continue
                key , value = line.split( ':' )
                key = key.strip()
                value = value.strip()
                result[ key ] = value
            fp.close()
    except IOError:
        logging( 'Error reading file %s' % ( path ) )
    return result

def parse_model_file( path ):
    result = []
    try:
        with open( path , 'r' ) as fp:
            block =  None
            lines = fp.readlines()
            for line in lines:
                line = line.rstrip()
                if line == '' or line[ 0 ] == '#':
                    continue        
                elif line[ 0 ] == '[':
                    if block:
                        result.append( block )
                    block = dict()
                    block[ 'type' ] = line.lstrip( '[' ).rstrip( ']' )
                    if block[ 'type' ] == 'convolutional':
                        block[ 'batch_normalize' ] = 0
                else:
                    key , value = line.split('=')
                    key = key.strip()
                    if key == 'type':
                        key = '_type'
                    value = value.strip()
                    block[ key ] = value
            if block:
                result.append( block )
            fp.close()
    except Exception:
        logging( 'Error reading file %s' % ( path ) )
    return result
#########################DATA AUGMENTATION#######################

def distort_image( im , hue , sat , val ):
    im = im.convert('HSV')
    cs = list( im.split( ))
    cs[ 1 ] = cs[ 1 ].point( lambda i: i * sat )
    cs[ 2 ] = cs[ 2 ].point( lambda i: i * val )

    def change_hue( x ):
        x += hue * 255
        if x > 255:
            x -= 255
        if x < 0:
            x += 255
        return x
    
    cs[ 0 ] = cs[ 0 ].point( change_hue )
    im = Image.merge( im.mode, tuple( cs ) )

    im = im.convert( 'RGB' )
    #constrain_image(im)
    return im

def rand_scale( s ):
    scale = random.uniform( 1 , s )
    if( random.randint( 1 , 10000 ) % 2 ): 
        return scale
    return 1./scale

def random_distort_image( im , hue , saturation , exposure ):
    dhue = random.uniform( -hue , hue )
    dsat = rand_scale( saturation )
    dexp = rand_scale( exposure )
    res = distort_image( im , dhue , dsat , dexp )
    return res

def data_augmentation( img , shape , jitter , hue , saturation , exposure ):
    oh = img.height
    ow = img.width
    
    dw =int( ow * jitter )
    dh =int( oh * jitter )

    pleft  = random.randint( -dw , dw )
    pright = random.randint( -dw , dw )
    ptop   = random.randint( -dh , dh )
    pbot   = random.randint( -dh , dh )

    swidth =  ow - pleft - pright
    sheight = oh - ptop - pbot

    sx = float( swidth )  / ow
    sy = float( sheight ) / oh
    
    flip = random.randint( 1 , 10000 ) % 2
    cropped = img.crop( ( pleft , ptop , pleft + swidth - 1 , ptop + sheight - 1 ) )

    dx = ( float( pleft ) / ow ) / sx
    dy = ( float( ptop ) / oh ) / sy

    sized = cropped.resize( shape )

    if flip: 
        sized = sized.transpose( Image.FLIP_LEFT_RIGHT )
    img = random_distort_image( sized , hue , saturation , exposure )
    
    return img, flip , dx , dy , sx , sy 

def fill_truth_detection( label_path , w , h , flip , dx , dy , sx , sy ):
    max_boxes = 50
    label = np.zeros( ( max_boxes , 5 ) )
    if os.path.getsize( label_path ):
        bs = np.loadtxt( label_path )
        if bs is None:
            return label
        bs = np.reshape( bs , ( -1 , 5 ) )
        cc = 0
        for i in range( bs.shape[ 0 ] ):
            x1 = bs[ i ][ 1 ] - bs[ i ][ 3 ] / 2
            y1 = bs[ i ][ 2 ] - bs[ i ][ 4 ] / 2
            x2 = bs[ i ][ 1 ] + bs[ i ][ 3 ] / 2
            y2 = bs[ i ][ 2 ] + bs[ i ][ 4 ] / 2
            
            x1 = min( 0.999 , max( 0 , x1 * sx - dx ) ) 
            y1 = min( 0.999 , max( 0 , y1 * sy - dy ) ) 
            x2 = min( 0.999 , max( 0 , x2 * sx - dx ) )
            y2 = min( 0.999 , max( 0 , y2 * sy - dy ) )
            
            bs[ i ][ 1 ] = ( x1 + x2 ) / 2
            bs[ i ][ 2 ] = ( y1 + y2 ) / 2
            bs[ i ][ 3 ] = ( x2 - x1 )
            bs[ i ][ 4 ] = ( y2 - y1 )

            if flip:
                bs[ i ][ 1 ] =  0.999 - bs[ i ][ 1 ] 
            
            if bs[ i ][ 3 ] < 0.001 or bs[ i ][ 4 ] < 0.001:
                continue
            label[ cc ] = bs[ i ]
            cc += 1
            if cc >= 50:
                break

    label = np.reshape( label , ( -1 ) )
    return label

def load_data_detection( img_path , label_path , shape , jitter, hue, saturation, exposure):
    img = Image.open( img_path ).convert('RGB')
    img , flip , dx , dy , sx , sy = data_augmentation( img , shape , jitter , hue , saturation , exposure )
    label = fill_truth_detection( label_path , img.width , img.height , flip , dx , dy , 1./sx , 1./sy )
    return img , label

###################### NO DATA AUGMENTATION ##########################

def read_truths( label_path ):
    if not os.path.exists( label_path ):
        return np.array( [] )
    if not os.path.getsize( label_path ):
        return np.array( [] )
    truths = np.loadtxt( label_path )
    truths = truths.reshape( truths.size / 5 , 5 ) 
    return truths

def read_truths_args( label_path , min_box_scale ):
    truths = read_truths( label_path )
    result = []
    for i in range( truths.shape[0] ):
        if truths[ i ][ 3 ] < min_box_scale:
            continue
        result.append( [ truths[ i ][ 0 ] , truths[ i ][ 1 ] , truths[ i ][ 2 ], truths[ i ][ 3 ], truths[ i ][ 4 ] ] )
    return np.array(new_truths)


