import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import torch.legacy.nn
from utils import *
from bk import db
from cfg import *

class MaxPoolStride1(nn.Module):
    def __init__(self):
        super(MaxPoolStride1, self).__init__()

    def forward(self, x):
        x = F.max_pool2d(F.pad(x, (0,1,0,1), mode='replicate'), 2, stride=1)
        return x

class Reorg(nn.Module):
    def __init__(self, stride=2):
        super(Reorg, self).__init__()
        self.stride = stride
    def forward(self, x):
        stride = self.stride
        assert(x.data.dim() == 4)
        B = x.data.size(0)
        C = x.data.size(1)
        H = x.data.size(2)
        W = x.data.size(3)
        assert(H % stride == 0)
        assert(W % stride == 0)
        ws = stride
        hs = stride
        x = x.view(B, C, H/hs, hs, W/ws, ws).transpose(3,4).contiguous()
        x = x.view(B, C, H/hs*W/ws, hs*ws).transpose(2,3).contiguous()
        x = x.view(B, C, hs*ws, H/hs, W/ws).transpose(1,2).contiguous()
        x = x.view(B, hs*ws*C, H/hs, W/ws)
        return x

class GlobalAvgPool2d(nn.Module):
    def __init__(self):
        super(GlobalAvgPool2d, self).__init__()

    def forward(self, x):
        N = x.data.size(0)
        C = x.data.size(1)
        H = x.data.size(2)
        W = x.data.size(3)
        x = F.avg_pool2d(x, (H, W))
        x = x.view(N, C)
        return x

# support route shortcut and reorg
class ConvNet( nn.Module ):
    def __init__( self , args ):
        super( ConvNet , self ).__init__()

        self.blocks = parse_model_file( args.model )
        del self.blocks[ 0 ]
        del self.blocks[ -1 ]

        self.models = self.create_network( self.blocks )

        self.weights_loaded = False
        if args.weights:
            self.load_weights( args.weights )

    def create_network( self , blocks ):
        models       = nn.ModuleList()
        prev_filters = 3
        out_filters  = []
        conv_id      = 0
        for block in blocks:

            if block[ 'type' ] == 'net':
                pass

            elif block[ 'type' ] == 'region':
                pass

            elif block['type'] == 'cost':
                pass

            elif block['type'] == 'convolutional':
                conv_id         = conv_id + 1
                batch_normalize = int( block[ 'batch_normalize' ] )
                filters         = int( block[ 'filters' ] )
                kernel_size     = int( block[ 'size' ] )
                stride          = int( block[ 'stride' ] )
                has_pad         = int( block[ 'pad' ] )
                pad             = ( kernel_size - 1 ) / 2 if has_pad else 0
                activation      = block[ 'activation' ]
                model           = nn.Sequential()

                if batch_normalize:
                    model.add_module( 'conv{0}'.format( conv_id ) , nn.Conv2d( prev_filters , filters , kernel_size , stride , pad , bias = False ) )
                    model.add_module( 'bn{0}'.format( conv_id ) , nn.BatchNorm2d( filters ) )
                else:
                    model.add_module( 'conv{0}'.format( conv_id ) , nn.Conv2d( prev_filters , filters , kernel_size , stride , pad ) )

                if activation == 'leaky':
                    model.add_module( 'leaky{0}'.format( conv_id ) , nn.LeakyReLU( 0.1 , inplace = True ) )
                elif activation == 'relu':
                    model.add_module( 'relu{0}'.format( conv_id), nn.ReLU( inplace = True ) )

                prev_filters     = filters
                out_filters.append( prev_filters )
                models.append( model )

            elif block[ 'type' ] == 'maxpool':
                pool_size       = int( block[ 'size' ] )
                stride          = int( block[ 'stride' ] )

                model = nn.MaxPool2d( pool_size , stride )
                #if stride > 1 :
                #    model = nn.MaxPool2d( pool_size , stride )
                #else:
                #    model = MaxPoolStride1()

                out_filters.append( prev_filters )
                models.append( model )

            elif block[ 'type' ] == 'avgpool':
                pool_size       = int( block[ 'size' ] )
                stride          = int( block[ 'stride' ] )
                model           = GlobalAvgPool2d()
                #model           = nn.AvgPool2d( poolSize , stride )
                out_filters.append( prev_filters )
                models.append( model )

            elif block['type'] == 'softmax':
                model = nn.Softmax()
                out_filters.append( prev_filters )
                models.append( model )

            elif block[ 'type' ] == 'reorg':
                stride       = int( block[ 'stride' ] )
                prev_filters = stride * stride * prev_filters
                model        = Reorg( stride )
                out_filters.append( prev_filters )
                models.append( model )
    
            elif block[ 'type' ] == 'route':
                layers          = block[ 'layers' ].split( ',' )
                ind             = len( models )
                layers          = [ int( i ) if int( i ) > 0 else int( i ) + ind for i in layers ]
                if len( layers ) == 1:
                    prev_filters = out_filters[ layers[ 0] ]
                elif len( layers ) == 2:
                    assert( layers[ 0 ] == ind - 1 )
                    prev_filters = out_filters[ layers[ 0 ] ] + out_filters[ layers[ 1 ] ]

                out_filters.append( prev_filters )
                assert( False )
                #model = torch.legacy.nn.Identity()
                models.append( model )

            elif block[ 'type' ] == 'shortcut':
                assert( len( out_filters ) != 0 )
                prev_filters     = out_filters[ -1 ]
                out_filters.append( prev_filters )
                assert( False )
                #model = torch.legacy.nn.Identity()
                models.append( model )

            elif block ['type' ] == 'connected':
                filters = int( block[ 'output' ] )
                if block[ 'activation' ] == 'linear':
                    model = nn.Linear( prev_filters , filters )
                elif block['activation'] == 'leaky':
                    model = nn.Sequential(
                               nn.Linear( prev_filters , filters ) ,
                               nn.LeakyReLU( 0.1 , inplace = True ) )
                elif block['activation'] == 'relu':
                    model = nn.Sequential(
                               nn.Linear( prev_filters , filters ) ,
                               nn.ReLU( inplace = True ) )
                else:
                    assert( False )
                prev_filters = filters
                out_filters.append( prev_filters )
                models.append( model )

    
        return models

    def forward( self , x ):
        outputs = dict()
        assert( len( self.blocks ) - len( self.models ) == 0 )
        for index in range( len( self.models ) ):
            block = self.blocks[ index ]
            model = self.models[ index ]

            if block[ 'type' ] == 'convolutional' or block[ 'type' ] == 'maxpool' or block[ 'type' ] == 'reorg' or block[ 'type' ] == 'avgpool' or block[ 'type' ] == 'softmax' or block[ 'type' ] == 'connected':
                x = model( x )
                outputs[ index ] = x

            elif block[ 'type' ] == 'route':
                layers = block[ 'layers' ].split( ',' )
                layers = [ int( i ) if int( i ) > 0 else int( i ) + ind for i in layers ]
                if len( layers ) == 1:
                    x = outputs[ layers[ 0 ] ]
                elif len(layers) == 2:
                    x1 = outputs[ layers[ 0 ] ]
                    x2 = outputs[ layers[ 1 ] ]
                    x = torch.cat( ( x1 , x2 ) , 1 )
                else:
                    raise ValueError( "no implemented" )
                outputs[ index ] = x

            elif block[ 'type' ] == 'shortcut':
                from_layer = int( block[ 'from' ] )
                activation = block[ 'activation' ]
                from_layer = from_layer if from_layer > 0 else from_layer + ind
                x1 = outputs[ from_layer ]
                x2 = outputs[ index - 1 ]
                x  = x1 + x2
                if activation == 'leaky':
                    x = F.leaky_relu( x , 0.1 , inplace = True )
                elif activation == 'relu':
                    x = F.relu( x , inplace = True )
                output[ index ] = x

            else:
                print('unknown type %s' % ( block[ 'type' ] ) )
        return x

    def load_weights( self, weight_file ):
        fp = open( weight_file , 'rb' )
        header = np.fromfile( fp , count=4 , dtype = np.int32 )
        #self.header = torch.from_numpy(header)
        #self.seen = self.header[3]
        buf = np.fromfile( fp , dtype = np.float32 )
        fp.close()

        start = 0
        ind = -1
        for block in self.blocks:
            if start >= buf.size:
                break
            ind = ind + 1
            if block['type'] == 'net':
                continue
            elif block['type'] == 'convolutional':
                model = self.models[ind]
                batch_normalize = int( block[ 'batch_normalize' ] )
                if batch_normalize:
                    start = load_conv_bn( buf , start, model[ 0 ] , model[ 1 ] )
                else:
                    start = load_conv( buf , start , model[ 0 ] )
            elif block[ 'type' ] == 'connected':
                model = self.models[ind]
                if block[ 'activation' ] != 'linear':
                    start = load_fc( buf , start , model[ 0 ] )
                else:
                    start = load_fc( buf , start , model )
            elif block['type'] == 'maxpool':
                pass
            elif block['type'] == 'reorg':
                pass
            elif block['type'] == 'route':
                pass
            elif block['type'] == 'shortcut':
                pass
            elif block['type'] == 'region':
                pass
            elif block['type'] == 'avgpool':
                pass
            elif block['type'] == 'softmax':
                pass
            elif block['type'] == 'cost':
                pass
            else:
                print('unknown type %s' % (block['type']))
        self.weights_loaded = True

    def save_weights(self, outfile, cutoff=0):
        if cutoff <= 0:
            cutoff = len(self.blocks)-1

        fp = open(outfile, 'wb')
        self.header[3] = self.seen
        header = self.header
        header.numpy().tofile(fp)

        ind = -1
        for blockId in range(1, cutoff+1):
            ind = ind + 1
            block = self.blocks[blockId]
            if block['type'] == 'convolutional':
                model = self.models[ind]
                batch_normalize = int(block['batch_normalize'])
                if batch_normalize:
                    save_conv_bn(fp, model[0], model[1])
                else:
                    save_conv(fp, model[0])
            elif block['type'] == 'connected':
                model = self.models[ind]
                if block['activation'] != 'linear':
                    save_fc(fc, model)
                else:
                    save_fc(fc, model[0])
            elif block['type'] == 'maxpool':
                pass
            elif block['type'] == 'reorg':
                pass
            elif block['type'] == 'route':
                pass
            elif block['type'] == 'shortcut':
                pass
            elif block['type'] == 'region':
                pass
            elif block['type'] == 'avgpool':
                pass
            elif block['type'] == 'softmax':
                pass
            elif block['type'] == 'cost':
                pass
            else:
                print('unknown type %s' % (block['type']))
        fp.close()
