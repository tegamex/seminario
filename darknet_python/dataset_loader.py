import os
import torch
import numpy as np
from torch.utils.data import Dataset
from PIL import Image
from bk import db
from utils import *

class DataSetLoader( Dataset ):
    def __init__( self , list_ids , images_path , labels_path , size = None, transform = None , target_transform = None , train = False ):
        super( Dataset , self ).__init__()
        
        self.list_ids    = list_ids
        self.images_path = images_path
        self.labels_path = labels_path

        self.data_count  = len( self.list_ids )
        self.transform = transform
        self.target_transform = target_transform
        self.size = size
        self.train = train

    def __len__( self ):
        return self.data_count

    def __getitem__( self , index ):
        assert index <= len( self ) , 'index range error'

        item_id  = self.list_ids[ index ]
        img_path = os.path.join( self.images_path , '%s.jpg' % ( item_id ) )
        label_path = os.path.join( self.labels_path , '%s.txt' % ( item_id ) )

        if self.train :
            jitter = 0.2
            hue = 0.1
            saturation = 1.5 
            exposure = 1.5
            img , label = load_data_detection( img_path , label_path , self.size , jitter , hue , saturation , exposure )
            label = torch.from_numpy(label)
        else:
            img = Image.open( img_path ).convert('RGB')
            if self.shape:
                img = img.resize( self.size )
    
            label = torch.zeros( 50 * 5 )
            tmp = torch.from_numpy( read_truths_args( label_path , 8.0 / img.width ).astype( 'float32' ) )
            tmp = tmp.view( -1 )
            tsz = tmp.numel()
            if tsz > 50 * 5 :
                label = tmp[ 0 : 50 * 5 ]
            elif tsz > 0:
                label[ 0 : tsz ] = tmp

        if self.transform is not None:
            img = self.transform( img )

        if self.target_transform is not None:
            label = self.target_transform( label )

        return ( img , label )
